from setuptools import setup

setup(
    name='telonum',
    version='0.0.2',
    description='Estimate the size of the telomere from ONT reads' ,
    install_requires=[
        "argparse", "biopython", "setuptools"],
    long_description=open('README.md').read(),
    packages=["telonum"],
    entry_points = {
        'console_scripts': ['telonum=telonum.telonum:main'],
    },
    scripts = ['scripts/repeat_to_gff.pl'],
)
