import argparse
from os.path import join, exists, isfile, isdir, abspath
from os import mkdir, listdir, chdir
import datetime
import sys
import subprocess
from shutil import copyfile
import pandas as pd
import gzip
from Bio import SeqIO
from multiprocessing import Pool

def print_err(message):
    print(f"{datetime.datetime.now()}: {message}")


def readIDToGFF(args):
    reads_file, readIDs, outdir, count = args
    reads_telo_ends = join(outdir, f"telo_reads_{count}.fasta")
    print_err(f'Writing files ({count})')
    with open(reads_telo_ends, 'wt') as outfile:
        if reads_file.split('.')[-1] == 'gz':
            with gzip.open(reads_file, 'rt') as infile:
                for record in SeqIO.parse(infile, 'fastq'):
                    if record.id in readIDs:
                        outfile.write(f">{record.id}\n")
                        outfile.write(f"{record.seq}\n")
        else:
            with open(reads_file, 'rt') as infile:
                for record in SeqIO.parse(infile, 'fastq'):
                    if record.id in readIDs:
                        outfile.write(f">{record.id}\n")
                        outfile.write(f"{record.seq}\n")
    chdir(outdir)
    reads_telo_ends = f"telo_reads_{count}.fasta"
    trf_dat_file = f'{reads_telo_ends}.1.1.2.80.5.200.2000.dat'
    gff_file = f'{trf_dat_file}.gff'
    print_err(f'Running Tandem Repeat Finder {count}')
    subprocess.run(f'trf {reads_telo_ends} 1 1 2 80 5 200 2000 -d -h', shell=True)
    print_err(f'Converting trf data file to gff {count}')
    subprocess.run(f'repeat_to_gff.pl {trf_dat_file}', shell=True)

    return [abspath(gff_file), abspath(reads_telo_ends)]
    

def getDescription(reads_file, telomere_file, outdir, threads):
    reads2ref_map = join(outdir, 'reads2ref.paf')
    combined_gff_file = join(outdir, 'combined_data.gff')
    combined_fasta_file = join(outdir, 'combined_telo_reads.fasta')
    output_file = join(outdir, f"telonum_stats.csv")
    # Map reads to teloemre
    print_err('Mapping reads to telomeres')
    subprocess.run(f'minimap2 -x map-ont -t {threads} {telomere_file} {reads_file} > {reads2ref_map}', shell=True)
    # Filter reads for telomeric reads
    tndm_df = pd.read_csv(reads2ref_map, sep='\t', names=[
        'qid', 'qlen', 'qstart', 'qend', 'strand',
        'sid', 'slen', 'sstart', 'send', 'matches',
        'alignment', 'score', 'a', 'b', 'c', 'd', 'e', 'f'], index_col=None)
    reads_telo_ends_IDs = set(tndm_df.loc[(((tndm_df['qstart'] < 1000) | (tndm_df['qend'] >= tndm_df['qlen']-1000)) & (tndm_df['qlen'] >= 10000)), 'qid'].unique())
    print_err(f'Writing {len(reads_telo_ends_IDs)} reads with telomere ends')
    readIDs_buckets = [[] for i in range(threads)]
    for count, readID in enumerate(reads_telo_ends_IDs):
        readIDs_buckets[count%threads].append(readID)
    non_empty_buckets = [bucket for bucket in readIDs_buckets if bucket]
    process_args = [[reads_file, readIDs, outdir, count] for count, readIDs in enumerate(non_empty_buckets)]
    p = Pool(threads)
    files = p.map(readIDToGFF, process_args)
    gff_files_string = ' '.join([i[0] for i in files])
    fasta_files_string = ' '.join([i[1] for i in files])
    subprocess.run(f'cat {fasta_files_string} > {combined_fasta_file}', shell=True)
    subprocess.run(f'echo "##gff-version 3" > {combined_gff_file}', shell=True)
    subprocess.run(f'cat {gff_files_string} | grep -v "##gff-version 3" >> {combined_gff_file}', shell=True)
    print_err('Estimating telomere stats')
    trf_df = pd.read_csv(combined_gff_file, sep='\t')
    headers = ['ID', 'PeriodSize', 'CopyNumber', 'PercentMatches', 'PercentIndels', 'Consensus']
    for c, header in enumerate(headers):
        trf_df[header] = trf_df['##gff-version 3'].apply(lambda x: x.split(';')[c].split('=')[-1])
    trf_df['repeat_len'] = trf_df['PeriodSize'].astype(float)*trf_df['CopyNumber'].astype(float)
    description = trf_df.loc[(trf_df['PercentMatches'].astype(int) >= 70) & (trf_df['PeriodSize'].astype(int) == 7), 'repeat_len'].describe()
    description.to_csv(output_file)

    return description


def main():
    parser = argparse.ArgumentParser(
        add_help=True)
    parser.add_argument(
        '-m', '--telomeres', metavar='String', type=str, action='store',
        dest='telomeres', required=True, help=('Telomere sequence file in fasta format'))
    parser.add_argument(
        '-o', '--outdir', metavar='String', type=str, action='store',
        dest='outdir', required=True)
    parser.add_argument(
        '-r', '--reads', action='store', dest='reads', required=True,
        help=('Reads file in fastq format'))
    parser.add_argument(
        '-t', '--threads', action='store',
        dest='threads', type=int, help=('Number of threads to use'), default=1)
    args = parser.parse_args()

    # Command line arguments
    telomere_file = args.telomeres
    outdir  = args.outdir
    reads_file = args.reads
    threads = args.threads
    # Check if input files exist and are files
    for f in [telomere_file, reads_file]:
        if not exists(f) or not isfile(f):
            print(f"{f} does not exist or is not a file!", file = sys.stderr)
            return 1

    # Make the output directory if it doesn't exist
    if not exists(outdir):
        mkdir(outdir)
    
    description = getDescription(reads_file, telomere_file, outdir, threads)
    print(description)