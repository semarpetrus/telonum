# TELONUM
Telonum is a pipeline that approximates telomere size of a genome from ONT and PacBio reads.
# 
## Installation
### Dependencies
1. Minimap2: can be installed with `conda install -c Bioconda minimap2`
2. Tandem Repeat Finder: can be installed with `conda install -c Bioconda trf`
### Pip Installation
Telonum can be installed with `pip install git+https://gitlab.com/semarpetrus/telonum.git`
## Inputs
1. **-m**: A fasta file containing telomere sequences. This preferably would contain the telomere base sequence repeated few times with both the forward and reverse strand.
2. **-o**: Ouput directory where all the intermediate files will be stored.
3. **-r**: A fastq file of ONT/PacBio reads.
4. **-t**: Number of threads to use.

## Outputs
1. **reads2ref.paf**: Alignment map of the reads onto the telomeres.
2. **combined_telo_reads.fasta**: Fasta file of the reads containing telomeres in the tail 1000 base pairs and are greater than 10kb.
3. **combined_data.gff**: A gff file containing tandem repeats and statistics for them.
4. **telonum_stats.csv**: A CSV file that contains telomere size estimation and other statistics.

## How It Works
1. Map reads to telomeres file with Minimap2
2. Extract reads that have the following criteria:
    * Larger than 10kb
    * Has telomere sequence in the first 1kb or the last 1kb
3. Run Tandem Repeat Finder on the extracted reads
4. Convert TRF data file to GFF file
5. Multiply "PeriodSize" and "CopyNumber" to get the repeat size for each repeat
6. Get repeat size statistics for repeats with 'PercentMatches' >= 70% 